---
typora-root-url: ./image
---

# java百万数据处理

优化方向：

1、提升程序运行的效率

2、jvm优化，防止内存溢出

## 1、提升效率

### 1-1、减少字段映射

**之前写法：**

逻辑：service通过调用mybatis-plus中的sysOrgService.list()接口获取组织中的所有数据

优点：可读性好、方便快捷

缺点：映射字段过多

```java
List<SysOrg> sysOrgList = sysOrgService.list();
```

**现在写法：**

逻辑：使用mybaits重写接口，获取数据

优点：自定义、映射字段少

缺点：书写繁琐

vip/xiaonuo/modular/simple/mapper/mapping/SimpleMapper.xml新增内容如下：

```xml
<resultMap id="sysOrgMap" type="vip.xiaonuo.sys.modular.org.entity.SysOrg">
    <id property="id" column="id"></id>
    <result property="name" column="name"></result>
 </resultMap>

<select id="sysOrgList" resultMap="sysOrgMap">
    SELECT
        id,
        name
    FROM
        sys_org
    ${ew.customSqlSegment}
 </select>
```

vip/xiaonuo/modular/simple/mapper/SimpleMapper.java新增内容如下：

```java
List<SysOrg> sysOrgList(@Param("ew") QueryWrapper<SysOrg> queryWrapper);
```

vip/xiaonuo/modular/simple/service/impl/SimpleServiceImpl.java中的调用修改：

```java
//            List<SysOrg> sysOrgList = sysOrgService.list();
            List<SysOrg> sysOrgList = simpleMapper.sysOrgList(new QueryWrapper<>());
```

### 1-2、多线程

**之前写法：**

vip/xiaonuo/modular/simple/service/impl/SimpleServiceImpl.java中的调用：

```java
List<SysOrg> sysOrgList = simpleMapper.sysOrgList(new QueryWrapper<>());
```

**现在写法：**

逻辑：多个线程，分页查询不同的list数据，来提升效率

vip/xiaonuo/modular/simple/service/impl/SimpleServiceImpl.java中修改：

```java
//使用线程安全的list     
CopyOnWriteArrayList<SysOrg> sysOrgList = new CopyOnWriteArrayList<SysOrg>();
// 组织机构条数
int total = sysOrgService.count();
// 线程数
int num = 6;
// 每个线程处理多少条数据
int size = (int) Math.ceil(1.0 * total/num);
//初始化线程数量 hutool写法
CountDownLatch countDownLatch=ThreadUtil.newCountDownLatch(num);
for (int i = 0; i < num; i++){
	int finalI = i * size;
     // hutool写法
    ThreadUtil.execute(()->{
        try {
        // mybaits-plus的写法 select * from limit 参数：i * size 参数：size
		// sysOrgList.addAll(sysOrgService.list(Wrappers.<SysOrg>lambdaQuery().last(
        //    StrUtil.format("limit {}, {}", Integer.toString(finalI), Integer.toString(size))           			// )));
           // mybaits的写法 4.1
           QueryWrapper<SysOrg> qw = new QueryWrapper<>();
           qw.last(StrUtil.format("limit {}, {}", Integer.toString(finalI), Integer.toString(size)));
           sysOrgList.addAll(simpleMapper.sysOrgList(qw));
         }catch (Throwable e){
           e.printStackTrace();
          }finally {
            // 递减锁存器的计数，如果计数到达零，则释放所有等待的线程
            countDownLatch.countDown();
          }
      });
}
try {
 	// 使当前线程在锁存器倒计数至零之前一直等待，除非线程被中断
	// 如果当前的计数为零，则此方法立即返回
	countDownLatch.await();
} catch (InterruptedException e) {
	e.printStackTrace();
}
```

## 2、防止内存溢出

### 2-1、修改内存回收机制

java -XX:+Use[gc_name]GC -Xmx2g -Xms32m -jar app.jar [sleep]

- gc_nam： 替换为特定的垃圾收集器类型
- Xms：初始化内存（最小）
- Xmx：最大内存
- sleep：是内存加载周期之间的间隔（以毫秒为单位），默认值为10

**将内存回收机制修改为G1**

- idea开发环境配置：

局部项目修改（推荐）：-XX:+UseG1GC -Xmx2g -Xms32m

![image-20221013003430987](/image-20221013003430987.png)

![image-20221013003749834](/image-20221013003749834.png)

全局修改方式：

![image-20221013004427797](/image-20221013004427797.png)

![image-20221013004542903](/image-20221013004542903.png)

- 生产环境下修改方式：

java -XX:+UseG1GC -Xmx2g -Xms32m -jar snowy.jar 0

参考文章：

https://blog.csdn.net/qiaobing1226/article/details/125209196

https://blog.csdn.net/m0_67390379/article/details/123741747

### 2-2、mybatis流式查询

1、第一种流式查询写法

vip/xiaonuo/modular/simple/mapper/mapping/SimpleMapper.xml

```xml
<select id="sysOrgStream" resultType="vip.xiaonuo.sys.modular.org.entity.SysOrg">
         SELECT
            id,
            name
        FROM
            sys_org
        ${ew.customSqlSegment}
    </select>
```

vip/xiaonuo/modular/simple/mapper/SimpleMapper.java

```java
/**
 * 第一种流式查询调用
 * @param queryWrapper
 * @param handler
 */
void sysOrgStream(@Param("ew") QueryWrapper<SysOrg> queryWrapper, ResultHandler<SysOrg> handler);
```

service调用方式

vip/xiaonuo/modular/simple/service/impl/SimpleServiceImpl.java

```java
simpleMapper.sysOrgStream(qw, new ResultHandler<SysOrg>() {
	@Override
	public void handleResult(ResultContext<? extends SysOrg> resultContext) {
		// sysOrgList.add(resultContext.getResultObject());
		sysOrgMap.put(resultContext.getResultObject().getId(), resultContext.getResultObject());
    }
});
```

```java
 // 流式查询调用简写（推荐）
simpleMapper.sysOrgStream(qw, ctx -> {
	// sysOrgList.add(ctx.getResultObject());
	sysOrgMap.put(ctx.getResultObject().getId(), ctx.getResultObject());
});
```

2、第二种流式查询写法

- xml文件

```xml
<select id="sysOrgStream" resultType="vip.xiaonuo.sys.modular.org.entity.SysOrg">
        SELECT
            id,
            name
        FROM
            sys_org
        ${ew.customSqlSegment}
    </select>
```

- mapper接口

```java
/**
 * 第二种流式查询调用
 * @param queryWrapper
 * @return
 */
@Options(fetchSize = Integer.MIN_VALUE,resultSetType = ResultSetType.FORWARD_ONLY)
Cursor<SysOrg> sysOrgStream(@Param("ew") QueryWrapper<SysOrg> queryWrapper);
```

- service调用（注意service方法种要加 @Transactional(readOnly = true) ）

```java
// 第二种流式查询调用
try (Cursor<SysOrg> cursor = simpleMapper.sysOrgStream(new QueryWrapper<>())) {
	// Iterator<SysOrg> iterator = cursor.iterator();
	cursor.forEach(sysOrg -> {
		// sysOrgList.add(sysOrg);
		sysOrgMap.put(sysOrg.getId(), sysOrg);
	});
}catch (IOException e) {
	e.printStackTrace();
}
```

- 这种方式要注意Druid属性配置：vip/xiaonuo/core/pojo/druid/DruidProperties.java（如果不进行设置控制台会打印一句警告日志，但不影响正常使用）

```java
private Boolean poolPreparedStatements = false;
private Integer maxPoolPreparedStatementPerConnectionSize = 0;
```

## 3、最终写法

service参考写法：

```java
    @Override
    public PageResult<Simple> page(SimpleParam simpleParam) {
        QueryWrapper<Simple> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(simpleParam)) {
            if (ObjectUtil.isNotEmpty(simpleParam.getName())) {
                queryWrapper.like("name", simpleParam.getName());
            }
            ……
        }
        Page<Simple> simplePage = this.page(PageFactory.defaultPage(), queryWrapper);
        // 查询数据库的数据
        if (ObjectUtil.isNotEmpty(simplePage)){
            Map<Long, SysOrg> sysOrgMap = new ConcurrentHashMap<>();
            // 组织机构条数
            int total = sysOrgService.count();
            // 线程数
            int num = 6;
            // 每个线程处理多少条数据
            int size = (int) Math.ceil(1.0 * total/num);
            //初始化线程数量 hutool写法
            CountDownLatch countDownLatch=ThreadUtil.newCountDownLatch(num);
            for (int i = 0; i < num; i++){
                int finalI = i * size;
                int finalI1 = i+1;
                // hutool写法
                ThreadUtil.execute(()->{
                    try {
                        // mybaits的写法 4.1
                        QueryWrapper<SysOrg> qw = new QueryWrapper<>();
                        qw.last(StrUtil.format("limit {}, {}", Integer.toString(finalI), Integer.toString(size)));
                        // 第一种：流式查询调用简写
                        simpleMapper.sysOrgStream(qw, ctx -> {
//                            sysOrgList.add(ctx.getResultObject());
                            sysOrgMap.put(ctx.getResultObject().getId(), ctx.getResultObject());
                        });
                    }catch (Throwable e){
                        e.printStackTrace();
                    }finally {
                        // 递减锁存器的计数，如果计数到达零，则释放所有等待的线程
                        countDownLatch.countDown();
                    }
                });
            }
            try {
                // 使当前线程在锁存器倒计数至零之前一直等待，除非线程被中断
                // 如果当前的计数为零，则此方法立即返回
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long middle3  = System.currentTimeMillis();
            List<Simple> simpleList = simplePage.getRecords();
            if (ObjectUtil.isNotEmpty(simpleList)){
                simpleList.forEach(item->{
                    // 组织机构的id
                    Long orgId = item.getOrgId();
                    // 通过组织机构的id去查询出，机构的名称
                    if(ObjectUtil.isNotNull(orgId)){
                        // 查询数据库 链接数据是很耗费性能
                        SysOrg sysOrg = sysOrgMap.get(orgId);
//                        SysOrg sysOrg = sysOrgService.getById(orgId);
                        if(ObjectUtil.isNotEmpty(sysOrg) && StrUtil.isNotBlank(sysOrg.getName())){
                            item.setOrgName(sysOrg.getName());
                        }
                    }
                });
            }
        }
        // sql
        return new PageResult<>(simplePage);
    }
```
