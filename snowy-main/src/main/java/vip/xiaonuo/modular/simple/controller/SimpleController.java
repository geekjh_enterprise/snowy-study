/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.simple.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.simple.param.SimpleParam;
import vip.xiaonuo.modular.simple.service.SimpleService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 单表业务控制器
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
@RestController
public class SimpleController {

    @Resource
    private SimpleService simpleService;

    /**
     * 查询单表业务
     *
     * @author abled
     * @date 2022-09-24 21:22:14
     */
    @Permission
    @GetMapping("/simple/page")
//    @BusinessLog(title = "单表业务_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(SimpleParam simpleParam) {
        return new SuccessResponseData(simpleService.page(simpleParam));
    }

    /**
     * 添加单表业务
     *
     * @author abled
     * @date 2022-09-24 21:22:14
     */
    @Permission
    @PostMapping("/simple/add")
    @BusinessLog(title = "单表业务_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(SimpleParam.add.class) SimpleParam simpleParam) {
            simpleService.add(simpleParam);
        return new SuccessResponseData();
    }

    /**
     * 删除单表业务，可批量删除
     *
     * @author abled
     * @date 2022-09-24 21:22:14
     */
    @Permission
    @PostMapping("/simple/delete")
    @BusinessLog(title = "单表业务_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(SimpleParam.delete.class) List<SimpleParam> simpleParamList) {
            simpleService.delete(simpleParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑单表业务
     *
     * @author abled
     * @date 2022-09-24 21:22:14
     */
    @Permission
    @PostMapping("/simple/edit")
    @BusinessLog(title = "单表业务_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(SimpleParam.edit.class) SimpleParam simpleParam) {
            simpleService.edit(simpleParam);
        return new SuccessResponseData();
    }

    /**
     * 查看单表业务
     *
     * @author abled
     * @date 2022-09-24 21:22:14
     */
    @Permission
    @GetMapping("/simple/detail")
    @BusinessLog(title = "单表业务_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(SimpleParam.detail.class) SimpleParam simpleParam) {
        return new SuccessResponseData(simpleService.detail(simpleParam));
    }

    /**
     * 单表业务列表
     *
     * @author abled
     * @date 2022-09-24 21:22:14
     */
    @Permission
    @GetMapping("/simple/list")
    @BusinessLog(title = "单表业务_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(SimpleParam simpleParam) {
        return new SuccessResponseData(simpleService.list(simpleParam));
    }

    /**
     * 导出系统用户
     *
     * @author abled
     * @date 2022-09-24 21:22:14
     */
    @Permission
    @GetMapping("/simple/export")
    @BusinessLog(title = "单表业务_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(SimpleParam simpleParam) {
        simpleService.export(simpleParam);
    }

}
