/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.simple.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.session.ResultHandler;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Repository;
import vip.xiaonuo.modular.simple.entity.Simple;
import vip.xiaonuo.modular.simple.result.SimpleResult;
import vip.xiaonuo.sys.modular.org.entity.SysOrg;

import java.util.List;
import java.util.Vector;
import java.util.WeakHashMap;

import org.apache.ibatis.annotations.Param;
import vip.xiaonuo.modular.simple.entity.Simple;
import vip.xiaonuo.modular.simple.result.SimpleResult;
import vip.xiaonuo.sys.modular.user.result.SysUserResult;

/**
 * 单表业务
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
public interface SimpleMapper extends BaseMapper<Simple> {
    /**
     * 获取单表业务分页列表
     *
     * @param page         分页参数
     * @param queryWrapper 查询参数
     * @return 查询分页结果
     * @author xuyuxiang
     * @date 2020/4/7 21:14
     */
    Page<SimpleResult> page(@Param("page") Page page, @Param("ew") QueryWrapper<?> queryWrapper);

    /**
     * 一次性获取全部数据
     * @param queryWrapper
     * @return
     */
    List<SysOrg> sysOrgList(@Param("ew") QueryWrapper<SysOrg> queryWrapper);

    /**
     * 第一种流式查询调用
     * @param queryWrapper
     * @param handler
     */
    void sysOrgStream(@Param("ew") QueryWrapper<SysOrg> queryWrapper, ResultHandler<SysOrg> handler);

    /**
     * 第二种流式查询调用
     * @param queryWrapper
     * @return
     */
    @Options(fetchSize = Integer.MIN_VALUE,resultSetType = ResultSetType.FORWARD_ONLY)
    Cursor<SysOrg> sysOrgStream(@Param("ew") QueryWrapper<SysOrg> queryWrapper);



}
