/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.simple.entity;

import com.baomidou.mybatisplus.annotation.*;
import vip.xiaonuo.core.pojo.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.*;
import cn.afterturn.easypoi.excel.annotation.Excel;

/**
 * 单表业务
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("bis_simple")
public class Simple extends BaseEntity {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 名称（必填）
     */
    @Excel(name = "名称（必填）")
    private String name;

    /**
     * 编码（必填唯一性）
     */
    @Excel(name = "编码（必填唯一性）")
    private String code;

    /**
     * 状态（0：冻结；1：正常）
     */
    @Excel(name = "状态（0：冻结；1：正常）")
    private Integer state;

    /**
     * 类型（字典项：类型a：A；类型b：B）
     */
    @Excel(name = "类型（字典项：类型a：A；类型b：B）")
    private String type;

    /**
     * 所属机构
     */
    @Excel(name = "所属机构")
    private Long orgId;

    /**
     * 扩展字段（json）
     */
    @Excel(name = "扩展字段（json）")
    private Object extField;

    /**
     * 附件
     */
    private String enclosure;

    /**
     * 备注信息（非必填）
     */
    @Excel(name = "备注信息（非必填）")
    private String postscript;

    /**
     * 负责人（编辑、删除、查看）
     */
    @Excel(name = "负责人（编辑、删除、查看）")
    private Long ponPer;

    /**
     * 相关人（查看）（多个用户的id，使用;进行分割）
     */
    @Excel(name = "相关人（查看）（多个用户的id，使用;进行分割）")
    private String relPer;


    /**
     * 机构的名称
     */
    // 数据库中不存在该字段
    @TableField(exist = false)
    private String orgName;
//
//    @TableField(exist = false)
//    private String ponPerName;
//
//    @TableField(exist = false)
//    private String relPerName;

}
