/** 您的业务接口文件全写在此文件夹下面，升级底座直接迁移代码即可 **/

/** 

     1、路径：_web/src/components/MyComp 所有封装的组件，升级时注意迁移 
     2、路径：_web/src/consts 所有的常量
     3、路径：_web/src/main.js 
        引入了js工具类（18-20）
        import XEUtils from 'xe-utils'
        import VXEUtils from 'vxe-utils'
        Vue.use(VXEUtils, XEUtils, { mounts: ['cookie'] })
     
**/