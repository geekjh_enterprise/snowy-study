
export const STATE = {
  // 冻结
  FREEZE: 0,
  // 正常
  NORMAL: 1,
  // 其他
  OTHER: 9
}

export const STATE_DIC = [
  {
    code: STATE.NORMAL,
    name: '正常'
  },
  {
    code: STATE.FREEZE,
    name: '冻结'
  },
  {
    code: STATE.OTHER,
    name: '其他'
  }
]
