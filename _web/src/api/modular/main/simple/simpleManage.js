import { axios } from '@/utils/request'

/**
 * 查询单表业务
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
export function simplePage (parameter) {
  return axios({
    url: '/simple/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 单表业务列表
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
export function simpleList (parameter) {
  return axios({
    url: '/simple/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加单表业务
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
export function simpleAdd (parameter) {
  return axios({
    url: '/simple/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑单表业务
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
export function simpleEdit (parameter) {
  return axios({
    url: '/simple/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除单表业务
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
export function simpleDelete (parameter) {
  return axios({
    url: '/simple/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出单表业务
 *
 * @author abled
 * @date 2022-09-24 21:22:14
 */
export function simpleExport (parameter) {
  return axios({
    url: '/simple/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
